# MHD turbulence closure models in planetary cores using AI

**SC leader:** <img src="https://gitlab.com/cheese5311126/CHEESE/-/raw/main/Logos/CNRS.png?inline=false" width="80">

**Partners:** <img src="https://gitlab.com/cheese5311126/CHEESE/-/raw/main/Logos/IPGP.png?inline=false" width="80">

**Codes:** [xSHELLS](https://gitlab.com/cheese5311126/codes/xshells/xshells)

**Target EuroHPC Architectures:** MN5, LUMI, Vega

**Type:** Basic research

## Description

This SC will run capacity geodynamo simulations accelerated by AI-based, subgrid-scale models and prototype
first the Neural Network (NN) on a series of 2D turbulent simulations of rotating convection using the pizza code, which is a
pseudo-spectral code whose solution strategy is similar to the one used in the xSHELLS code. In order to implement and
validate the NN, the level of rotating turbulence will be gradually increased until a Reynolds number equals 10⁵, with a
Rossby number equal to 10⁻⁴, amounting to DNS 1200³. At each step, upon successful validation of the NN framework with
the prototype, we will next couple it with the xSHELLS code and assess its accuracy using a high-fidelity simulation run
with 3 resolutions DNS: 200³, 500³ and 1200³. For each resolution, about 100 (at coarse resolution), 30 (at medium
resolution) and 10 (at large resolution) runs will be made to explore a variety of regimes with and without subgrid-scale
models: some runs without magnetic field, some with magnetic field but no scale separation, some with scale separation for
the magnetic field, some with separate chemical and temperature fields and scale separation. The idea is to simulate the
Earth’s core at the equivalent of a DNS with resolution 4000³ or more, at about 1/1000 of the cost. The scientific gain is
important, such a subgrid-scale model could be applied to many geophysical and astrophysical models, resulting in a
speed-up of current and future calculations by orders of magnitude.

<img src="SC7.1.png" width="600">

**Figure 3.11.1.** Graphical abstract of SC7.1 showing the vorticity of a two-dimensional
rotating convection simulation.

## Expected results

In the case the model is relevant for other parameters (as suggested by the “path theory” for the Earth’s
core), one could readily simulate the Earth’s core at its nominal parameters. Orders of magnitude improvement in
time-to-simulation is also what is needed to explore the magnetic field reversals at realistic parameters, and to perform
realistic parametric studies. The numerical datasets produced will be made available to the community, each comprising a
catalog of time series of the quantity of interest, time-dependent 2D maps of the field and flow at the core surface, and a
number of full 3D snapshots for more detailed post-processing.